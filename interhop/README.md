# On the APPLICATION server

## First deployment

Official Cryptpad Documentation : https://docs.cryptpad.org/en/admin_guide/installation.html

### Clone the repos

```shell
git clone --recurse-submodules git@framagit.org:interhop/hds/cryptpad-docker.git
```

### Build the image

```shell
# update cryptpad source
cd cryptpad-docker/cryptpad
docker build -t cryptpad/cryptpad:latest .
```

### Edit environment variables

```shell
cd interhop
cp env-example env-prod
```

#### In ```env_prod``` file, CHECK/MODIFY the values
```
CPAD_MAIN_DOMAIN=cryptpad.hds.interhop.org
CPAD_SANDBOX_DOMAIN=cryptpad.sandbox.hds.interhop.org
DATA_PATH # where the data will be persisted
HTTP_PORT # the local port to be used to bind cryptpad
```

### Configure cryptpad

```shell
mkdir -p $DATA_PATH/data/
cp -r customize $DATA_PATH/data/
```

#### In `application_config.js` file 
Put a random value for loginsalt `AppConfig.loginSalt = '';`

#### In ```config.js``` file, CHECK/MODIFY the values
```bash
export CPAD_MAIN_DOMAIN=cryptpad.hds.interhop.org
export CPAD_SANDBOX_DOMAIN=cryptpad.sandbox.hds.interhop.org
export ADMIN_KEYS='"[the-admin-keys]",'
envsubst '$CPAD_MAIN_DOMAIN,$CPAD_SANDBOX_DOMAIN,$ADMIN_KEYS' < config.js > $DATA_PATH/config/config.js
```

#### Set the file owner
```bash
chown -R 4001:4001 $DATA_PATH
```

###  Start up cryptpad

The owner has to be changed after first startup:

```shell
docker compose --env-file env-prod up -d
docker compose down
chown -R 4001:4001 $DATA_PATH
docker compose --env-file env-prod up -d
```

### Next Step

The administrator users must create an account and share their public key.
Then you have to modify the ```config.js``` file as follows:
```shell
adminKeys: [
        "[cryptpad-user1@my.awesome.website/YZgXQxKR0Rcb6r6CmxHPdAGLVludrAF2lEnkbx1vVOo=]",
],
```

Each time ```config.js``` or ```application_config.js``` are modified you have to restart the docker.

```shell
docker compose down
docker compose --env-file env-prod up -d
```



# Configure the PROXY server

- Specify nginx proxy config and put the resulting template in the site enabled:
```
export CPAD_MAIN_DOMAIN=cryptpad.hds.interhop.org
export CPAD_SANDBOX_DOMAIN=cryptpad.sandbox.hds.interhop.org
export SERVER_IP=localhost
export HTTP_PORT=3000

envsubst '$CPAD_MAIN_DOMAIN,$CPAD_SANDBOX_DOMAIN,$SERVER_IP,$HTTP_PORT' < nginx-cryptpad-reverse-proxy.conf
```
- run ```certbot --nginx -d cryptpad.hds.interhop.org -d cryptpad.sandbox.hds.interhop.org```
- ```nginx -t```
- `systemctl reload nginx`



## Instruction to upgrade

### Upgrade the configs
See "Configure cryptpad"


### Upgrade the proxy
See "Configure the PROXY server"

### Upgrade cryptpad
```shell
git pull
cd cryptpad-docker/cryptpad
git submodule update --init --recursive
docker build -t cryptpad/cryptpad:latest .
cd cryptpad-docker/interhop
docker compose down
docker compose --env-file env-prod up -d
```

